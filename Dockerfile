FROM maven:3-eclipse-temurin-17-alpine
ADD src /app/src
ADD pom.xml /app
WORKDIR /app
RUN mvn clean package
CMD ["java", "-jar", "target/movieAPI-0.0.1-SNAPSHOT.jar"]
