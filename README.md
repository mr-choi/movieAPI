
# Movie API

This project is the third assignment of the backend part of the full stack
Java developer course at Noroff. The goal of this assignment is to use
Spring boot, Spring web, JPA to develop a simple API with general crud
functionalities.



## Installation

You will need git to clone this project.
To compile and run the local development version, you will need docker.

To run the project, open a terminal, go to the root directory of the project,
and run

```
docker-compose up
```

### Profiles
The project has 3 profiles: testing (test), development (dev), and
production (prod).
- The testing profile is used to run both unit and integration tests.
  This will be done using maven. Important is to explicitly set the profile
  when running maven:

  ```
  mvn test -Dspring.profiles.active=test     # (unit tests)
  mvn verify -Dspring.profiles.active=test   # (integration tests)
  ```
- The development profile is for developing and debugging the project.
  This profile is used when `docker-compose up` is used (see above).
  The development profile provides the possibility to remotely debug the
  project.
  By default, the movie API will be exposed at port 8008.
  When running the development profile, a docker container with postgres
  will run as well. This postgres server will be exposed at port 2345.
  The ports can be changed in the `docker-compose.yml` file.
- The production profile is meant for deployment on Heroku.
  Unlike in the development profile, the database will not get seeded.
  To deploy on Heroku, the application must have a postgres addon added.
  Using the Heroku CLI, the application can be deployed by doing the following:
  ```
  heroku login
  heroku container: login
  heroku container:push web --app <app-name>
  heroku container:release web --app <app-name>
  ```
## Usage

When running the local development version, the documentation of the API
can be found by opening the following link
[http://localhost:8008/swagger-ui/index.html](http://localhost:8008/swagger-ui/index.html).
   
## Authors

- [@mr-choi](https://www.gitlab.com/mr-choi)


## Contributing

This project is a personal assignment so I do not expect contributions.
You are free, however, to make use of the project under the MIT license
(see below).


## License

[MIT](https://choosealicense.com/licenses/mit/)

