insert into franchise (description, name) VALUES ('The Marvel universe', 'Marvel');
insert into franchise (description, name) VALUES ('The Lord of the rings universe by Tolkien', 'Lord of the Rings');

insert into movie
    (director, genre, release_year, title, franchise_id)
VALUES
    ('Jon Watts', 'Action, Adventure, Fantasy', 2021, 'Spider-Man: No Way Home', 1);
insert into movie
    (director, genre, release_year, title, franchise_id)
VALUES
    ('Jon Watts', 'Action, Adventure, Sci-Fi', 2017, 'Spider-Man: Homecoming', 1);
insert into movie
    (director, genre, release_year, title, franchise_id)
values
    ('Peter Jackson', 'Adventure, Fantasy', 2012, 'The Hobbit: An Unexpected Journey', 2);

insert into character
    (alias, full_name, gender)
VALUES
    ('Spider-Man', 'Peter Parker', 'MALE');

insert into character
    (alias, full_name, gender)
VALUES
    ('Doctor Octopus', 'Otto Octavius', 'MALE');

insert into character
    (alias, full_name, gender)
VALUES
    ('The Hobbit', 'Bilbo Baggins', 'MALE');

insert into movie_character (movie_id, character_id) VALUES (1,1);
insert into movie_character (movie_id, character_id) VALUES (1,2);
insert into movie_character (movie_id, character_id) VALUES (2,1);
insert into movie_character (movie_id, character_id) VALUES (3,3);
