package nl.mrchoi.movieapi.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "character")
public class Character implements Identifiable<Integer> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "full_name", nullable = false, length = 50)
    private String fullName;

    @Column(name = "alias", length = 25)
    private String alias;

    @Enumerated(EnumType.STRING)
    @Column(name = "gender", nullable = false)
    private Gender gender;

    @Column(name = "picture_url", length = 200)
    private String pictureUrl;

    @ManyToMany(mappedBy = "characters")
    private Set<Movie> movies = new java.util.LinkedHashSet<>();
}