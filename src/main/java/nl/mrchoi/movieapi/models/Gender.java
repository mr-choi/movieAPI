package nl.mrchoi.movieapi.models;

public enum Gender {
    MALE,
    FEMALE,
    OTHER
}
