package nl.mrchoi.movieapi.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "movie")
public class Movie implements Identifiable<Integer> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "title", nullable = false, length = 50)
    private String title;

    @Column(name = "genre", nullable = false, length = 50)
    private String genre;

    @Column(name = "release_year", nullable = false)
    private Integer releaseYear;

    @Column(name = "director", nullable = false, length = 50)
    private String director;

    @Column(name = "picture_url", length = 200)
    private String pictureUrl;

    @Column(name = "trailer_url", length = 200)
    private String trailerUrl;

    @ManyToMany
    @JoinTable(name = "movie_character",
            joinColumns = @JoinColumn(name = "movie_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "character_id", referencedColumnName = "id"))
    private Set<Character> characters = new java.util.LinkedHashSet<>();

    @ManyToOne
    @JoinColumn(name = "franchise_id")
    private Franchise franchise;
}