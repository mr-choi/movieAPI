package nl.mrchoi.movieapi.models.dtos;

import lombok.Data;

import java.io.Serializable;
import java.util.Set;

@Data
public class FranchiseDto implements Serializable {
    private Integer id;
    private String name;
    private String description;
    private Set<Integer> movies;
}
