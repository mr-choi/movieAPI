package nl.mrchoi.movieapi.models.dtos;

import lombok.Data;
import nl.mrchoi.movieapi.models.Gender;

import java.io.Serializable;
import java.util.Set;

@Data
public class CharacterDto implements Serializable {
    private Integer id;
    private String fullName;
    private String alias;
    private Gender gender;
    private String pictureUrl;
    private Set<Integer> movies;
}
