package nl.mrchoi.movieapi.models.dtos;

import lombok.Data;

import java.io.Serializable;
import java.util.Set;

@Data
public class MovieDto implements Serializable {
    private Integer id;
    private String title;
    private String genre;
    private Integer releaseYear;
    private String director;
    private String pictureUrl;
    private String trailerUrl;
    private Set<Integer> characters;
    private Integer franchise;
}
