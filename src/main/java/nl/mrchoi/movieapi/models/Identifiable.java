package nl.mrchoi.movieapi.models;

/**
 * Generic interface meant for entities with an id.
 * @param <ID> The type of id the entity uses
 */
public interface Identifiable<ID> {
    ID getId();
}
