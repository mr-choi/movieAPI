package nl.mrchoi.movieapi.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import nl.mrchoi.movieapi.mappings.CharacterMapper;
import nl.mrchoi.movieapi.models.Character;
import nl.mrchoi.movieapi.models.dtos.CharacterDto;
import nl.mrchoi.movieapi.services.CharacterService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping(path = "api/v1/characters")
public class CharacterController extends CrudController<Integer,CharacterDto,Character,CharacterMapper,CharacterService> {

    public CharacterController(CharacterService service, CharacterMapper mapper) {
        super(service, mapper, "api/v1/characters/");
    }

    @Operation(summary = "Get all characters")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Found succesfully", content = @Content)
    })
    @GetMapping
    public ResponseEntity<Set<CharacterDto>> getAll() {
        return super.getAll();
    }

    @Operation(summary = "Get character of the given id")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Found successfully", content = @Content),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content)
    })
    @GetMapping("{id}")
    public ResponseEntity<CharacterDto> getById(@PathVariable Integer id) {
        return super.getById(id);
    }

    @Operation(summary = "Add new character",
            description = "The details of the character should be provided in the request body." +
                    "The request body should NOT contain the id. If that is the case, the response will be error 400.")
    @ApiResponses({
            @ApiResponse(responseCode = "201", description = "Successfully added", content = @Content),
            @ApiResponse(responseCode = "400", description = "Id in body", content = @Content)
    })
    @PostMapping
    public ResponseEntity<CharacterDto> add(@RequestBody CharacterDto characterDto) {
        return super.add(characterDto);
    }

    @Operation(summary = "Update existing character of given id",
            description = "The id of the character to be updated must be specified in the body." +
                    "If this is not the case, the response will be 404.")
    @ApiResponses({
            @ApiResponse(responseCode = "201", description = "Successfully updated", content = @Content),
            @ApiResponse(responseCode = "404", description = "Movie not found or not specified in body", content = @Content)
    })
    @PutMapping
    public ResponseEntity<CharacterDto> update(@RequestBody CharacterDto characterDto) {
        return super.update(characterDto);
    }

    @Operation(summary = "Delete a character of given id")
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "Successfully deleted", content = @Content),
    })
    @DeleteMapping("{id}")
    public ResponseEntity<CharacterDto> delete(@PathVariable Integer id) {
        return super.delete(id);
    }
}
