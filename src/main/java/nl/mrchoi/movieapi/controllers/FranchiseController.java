package nl.mrchoi.movieapi.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import nl.mrchoi.movieapi.mappings.CharacterMapper;
import nl.mrchoi.movieapi.mappings.FranchiseMapper;
import nl.mrchoi.movieapi.mappings.MovieMapper;
import nl.mrchoi.movieapi.models.Franchise;
import nl.mrchoi.movieapi.models.dtos.CharacterDto;
import nl.mrchoi.movieapi.models.dtos.FranchiseDto;
import nl.mrchoi.movieapi.models.dtos.MovieDto;
import nl.mrchoi.movieapi.services.FranchiseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "api/v1/franchises")
public class FranchiseController extends CrudController<Integer,FranchiseDto,Franchise,FranchiseMapper,FranchiseService> {

    private final MovieMapper movieMapper;
    private final CharacterMapper characterMapper;

    public FranchiseController(FranchiseService service, FranchiseMapper mapper, MovieMapper movieMapper,
                               CharacterMapper characterMapper) {
        super(service, mapper, "api/v1/franchises/");
        this.movieMapper = movieMapper;
        this.characterMapper = characterMapper;
    }

    @Operation(summary = "Get all franchises")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Found succesfully", content = @Content)
    })
    @GetMapping
    public ResponseEntity<Set<FranchiseDto>> getAll() {
        return super.getAll();
    }

    @Operation(summary = "Get franchise of the given id")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Found successfully", content = @Content),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content)
    })
    @GetMapping("{id}")
    public ResponseEntity<FranchiseDto> getById(@PathVariable Integer id) {
        return super.getById(id);
    }

    @Operation(summary = "Get all movies in a franchise of the given id")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Franchise with movies found successfully", content = @Content),
            @ApiResponse(responseCode = "404", description = "Franchise not found", content = @Content)
    })
    @GetMapping("{id}/movies")
    public ResponseEntity<Set<MovieDto>> getMoviesByFranchiseId(@PathVariable Integer id) {
        return ResponseEntity.ok(
                service.getMoviesFromFranchiseId(id).stream().map(movieMapper::toDto).collect(Collectors.toSet())
        );
    }

    @Operation(summary = "Get all characters in a franchise of the given id")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Franchise with characters found successfully", content = @Content),
            @ApiResponse(responseCode = "404", description = "Franchise not found", content = @Content)
    })
    @GetMapping("{id}/characters")
    public ResponseEntity<Set<CharacterDto>> getCharactersByFranchiseId(@PathVariable Integer id) {
        return ResponseEntity.ok(
                service.getCharactersFromFranchiseId(id).stream().map(characterMapper::toDto)
                        .collect(Collectors.toSet())
        );
    }

    @Operation(summary = "Add new franchise",
            description = "The details of the franchise should be provided in the request body." +
                    "The request body should NOT contain the id. If that is the case, the response will be error 400.")
    @ApiResponses({
            @ApiResponse(responseCode = "201", description = "Successfully added", content = @Content),
            @ApiResponse(responseCode = "400", description = "Id in body", content = @Content)
    })
    @PostMapping
    public ResponseEntity<FranchiseDto> add(@RequestBody FranchiseDto franchiseDto) {
        return super.add(franchiseDto);
    }

    @Operation(summary = "Update existing franchise of given id",
            description = "The id of the franchise to be updated must be specified in the body." +
                    "If this is not the case, the response will be 404.")
    @ApiResponses({
            @ApiResponse(responseCode = "201", description = "Successfully updated", content = @Content),
            @ApiResponse(responseCode = "404", description = "Movie not found or not specified in body", content = @Content)
    })
    @PutMapping
    public ResponseEntity<FranchiseDto> update(@RequestBody FranchiseDto franchiseDto) {
        return super.update(franchiseDto);
    }

    @Operation(summary = "Update movies in a franchise of given id",
            description = "You need to specify a list of movie ids in the body." +
                    "Movie ids that do not exist will simply be ignored." +
                    "Beware: the operation fully replaces all the movies of the franchise.")
    @ApiResponses({
            @ApiResponse(responseCode = "201", description = "Successfully updated", content = @Content),
            @ApiResponse(responseCode = "404", description = "Franchise not found", content = @Content)
    })
    @PutMapping("{id}/movies")
    public ResponseEntity<FranchiseDto> setMoviesForFranchiseId(@PathVariable Integer id,
                                                            @RequestBody Set<Integer> movieIds) {
        return ResponseEntity.created(URI.create(this.location + id + "/movies"))
                .body(mapper.toDto(service.setMoviesForFranchiseId(id, movieIds)));
    }

    @Operation(summary = "Delete a franchise of given id")
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "Successfully deleted", content = @Content),
    })
    @DeleteMapping("{id}")
    public ResponseEntity<FranchiseDto> delete(@PathVariable Integer id) {
        return super.delete(id);
    }
}
