package nl.mrchoi.movieapi.controllers;

import lombok.AllArgsConstructor;
import nl.mrchoi.movieapi.mappings.DtoEntityMapper;
import nl.mrchoi.movieapi.models.Identifiable;
import nl.mrchoi.movieapi.services.CrudService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.ResponseEntity;

import java.net.URI;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Generic REST controller that is extended by any controller that wants to do crud operations via http.
 * @param <ID> The data type of entity id.
 * @param <DTO> The type of DTO used.
 * @param <T> The type of database entity.
 * @param <M> The type of mapper used to convert between DTO and entity.
 * @param <S> The type service that handles the business logic.
 */
@AllArgsConstructor
public abstract class CrudController<
        ID,
        DTO,
        T extends Identifiable<ID>,
        M extends DtoEntityMapper<DTO,T>,
        S extends CrudService<ID,T,? extends JpaRepository<T,ID>>> {
    protected S service;
    protected M mapper;
    protected String location;

    public ResponseEntity<Set<DTO>> getAll() {
        return ResponseEntity.ok(service.findAll().stream().map(entity -> mapper.toDto(entity)).collect(Collectors.toSet()));
    }

    public ResponseEntity<DTO> getById(ID id) {
        return ResponseEntity.ok(mapper.toDto(service.findById(id)));
    }

    public ResponseEntity<DTO> add(DTO dto) {
        T entity = service.add(mapper.toEntity(dto));
        return ResponseEntity.created(URI.create(location + entity.getId())).body(mapper.toDto(entity));
    }

    public ResponseEntity<DTO> update(DTO dto) {
        T entity = service.update(mapper.toEntity(dto));
        return ResponseEntity.created(URI.create(location + entity.getId())).body(mapper.toDto(entity));
    }

    public ResponseEntity<DTO> delete(ID id) {
        service.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
