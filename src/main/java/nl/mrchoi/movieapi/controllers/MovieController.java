package nl.mrchoi.movieapi.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import nl.mrchoi.movieapi.mappings.CharacterMapper;
import nl.mrchoi.movieapi.mappings.MovieMapper;
import nl.mrchoi.movieapi.models.Movie;
import nl.mrchoi.movieapi.models.dtos.CharacterDto;
import nl.mrchoi.movieapi.models.dtos.MovieDto;
import nl.mrchoi.movieapi.services.MovieService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "api/v1/movies")
public class MovieController extends CrudController<Integer,MovieDto,Movie,MovieMapper,MovieService> {

    private final CharacterMapper characterMapper;
    public MovieController(MovieService service, MovieMapper mapper, CharacterMapper characterMapper) {
        super(service, mapper, "api/v1/movies/");
        this.characterMapper = characterMapper;
    }

    @Operation(summary = "Get all movies")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Found succesfully", content = @Content)
    })
    @GetMapping
    public ResponseEntity<Set<MovieDto>> getAll() {
        return super.getAll();
    }

    @Operation(summary = "Get movie of the given id")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Found successfully", content = @Content),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content)
    })
    @GetMapping("{id}")
    public ResponseEntity<MovieDto> getById(@PathVariable Integer id) {
        return super.getById(id);
    }

    @Operation(summary = "Get all characters in a movie of the given id")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Movie with characters found successfully", content = @Content),
            @ApiResponse(responseCode = "404", description = "Movie not found", content = @Content)
    })
    @GetMapping("{id}/characters")
    public ResponseEntity<Set<CharacterDto>> getCharacters(@PathVariable Integer id) {
        return ResponseEntity.ok(
                service.getCharactersFromMovieId(id).stream().map(characterMapper::toDto)
                        .collect(Collectors.toSet())
        );
    }

    @Operation(summary = "Add new movie",
            description = "The details of the movie should be provided in the request body." +
                    "The request body should NOT contain the id. If that is the case, the response will be error 400.")
    @ApiResponses({
            @ApiResponse(responseCode = "201", description = "Successfully added", content = @Content),
            @ApiResponse(responseCode = "400", description = "Id in body", content = @Content)
    })
    @PostMapping
    public ResponseEntity<MovieDto> add(@RequestBody MovieDto movieDto) {
        return super.add(movieDto);
    }

    @Operation(summary = "Update existing movie of given id",
            description = "The id of the movie to be updated must be specified in the body." +
                    "If this is not the case, the response will be 404.")
    @ApiResponses({
            @ApiResponse(responseCode = "201", description = "Successfully updated", content = @Content),
            @ApiResponse(responseCode = "404", description = "Movie not found or not specified in body", content = @Content)
    })
    @PutMapping
    public ResponseEntity<MovieDto> update(@RequestBody MovieDto movieDto) {
        return super.update(movieDto);
    }

    @Operation(summary = "Update characters in a movie of given id",
            description = "You need to specify a list of characters ids in the body." +
                    "Character ids that do not exist will simply be ignored." +
                    "Beware: the operation fully replaces all the characters of the movie.")
    @ApiResponses({
            @ApiResponse(responseCode = "201", description = "Successfully updated", content = @Content),
            @ApiResponse(responseCode = "404", description = "Movie not found", content = @Content)
    })
    @PutMapping("{id}/characters")
    public ResponseEntity<MovieDto> setCharactersForMovieId(@PathVariable Integer id,
                                                            @RequestBody Set<Integer> characterIds) {
        return ResponseEntity.created(URI.create(this.location + id + "/characters"))
                .body(mapper.toDto(service.setCharactersForMovieId(id, characterIds)));
    }

    @Operation(summary = "Delete a movie of given id")
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "Successfully deleted", content = @Content),
    })
    @DeleteMapping("{id}")
    public ResponseEntity<MovieDto> delete(@PathVariable Integer id) {
        return super.delete(id);
    }
}
