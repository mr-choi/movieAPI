package nl.mrchoi.movieapi.repositories;

import nl.mrchoi.movieapi.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FranchiseRepository extends JpaRepository<Franchise, Integer> {
}