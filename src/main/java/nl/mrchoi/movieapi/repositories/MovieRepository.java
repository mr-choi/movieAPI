package nl.mrchoi.movieapi.repositories;

import nl.mrchoi.movieapi.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovieRepository extends JpaRepository<Movie, Integer> {
}