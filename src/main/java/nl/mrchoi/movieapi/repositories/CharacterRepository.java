package nl.mrchoi.movieapi.repositories;

import nl.mrchoi.movieapi.models.Character;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CharacterRepository extends JpaRepository<Character, Integer> {
}