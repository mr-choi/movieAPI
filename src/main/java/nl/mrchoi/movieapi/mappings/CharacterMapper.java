package nl.mrchoi.movieapi.mappings;

import nl.mrchoi.movieapi.models.Character;
import nl.mrchoi.movieapi.models.Movie;
import nl.mrchoi.movieapi.models.dtos.CharacterDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

@Mapper(componentModel = "spring")
public abstract class CharacterMapper implements DtoEntityMapper<CharacterDto, Character> {

    @Autowired
    protected IdSetMovieSetConverter movieConverter;

    @Mapping(target = "movies", source = "movies", qualifiedByName = "toMovieId")
    public abstract CharacterDto toDto(Character character);
    @Mapping(target = "movies", source = "movies", qualifiedByName = "toMovieEntity")
    public abstract Character toEntity(CharacterDto characterDto);

    @Named("toMovieId")
    Set<Integer> toMovieId(Set<Movie> movies) {
        return movieConverter.toIdSet(movies);
    }

    @Named("toMovieEntity")
    Set<Movie> toMovieEntity(Set<Integer> ids) {
        return movieConverter.toEntitySet(ids);
    }
}
