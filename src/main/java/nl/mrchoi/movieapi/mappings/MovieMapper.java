package nl.mrchoi.movieapi.mappings;

import nl.mrchoi.movieapi.models.Character;
import nl.mrchoi.movieapi.models.Franchise;
import nl.mrchoi.movieapi.models.Movie;
import nl.mrchoi.movieapi.models.dtos.MovieDto;
import nl.mrchoi.movieapi.services.FranchiseService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

@Mapper(componentModel = "spring")
public abstract class MovieMapper implements DtoEntityMapper<MovieDto,Movie> {

    @Autowired
    protected IdSetCharacterSetConverter characterConverter;
    @Autowired
    protected FranchiseService franchiseService;

    @Mapping(target = "franchise", source = "franchise.id")
    @Mapping(target = "characters", source = "characters", qualifiedByName = "toCharacterId")
    public abstract MovieDto toDto(Movie movie);

    @Mapping(target = "franchise", source = "franchise", qualifiedByName = "toFranchiseEntity")
    @Mapping(target = "characters", source = "characters", qualifiedByName = "toCharacterEntity")
    public abstract Movie toEntity(MovieDto movieDto);

    @Named("toCharacterId")
    Set<Integer> toCharacterId(Set<Character> characters) {
        return characterConverter.toIdSet(characters);
    }

    @Named("toCharacterEntity")
    Set<Character> toCharacterEntity(Set<Integer> integers) {
        return characterConverter.toEntitySet(integers);
    }

    @Named("toFranchiseEntity")
    Franchise toFranchiseEntity(Integer id) {
        return (id == null) ? null : franchiseService.findById(id);
    }
}
