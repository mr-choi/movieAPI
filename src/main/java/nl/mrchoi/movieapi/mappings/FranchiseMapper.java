package nl.mrchoi.movieapi.mappings;

import nl.mrchoi.movieapi.models.Franchise;
import nl.mrchoi.movieapi.models.Movie;
import nl.mrchoi.movieapi.models.dtos.FranchiseDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

@Mapper(componentModel = "spring")
public abstract class FranchiseMapper implements DtoEntityMapper<FranchiseDto, Franchise> {

    @Autowired
    protected IdSetMovieSetConverter movieConverter;

    @Mapping(target = "movies", source = "movies", qualifiedByName = "toMovieId")
    public abstract FranchiseDto toDto(Franchise franchise);
    @Mapping(target = "movies", source = "movies", qualifiedByName = "toMovieEntity")
    public abstract Franchise toEntity(FranchiseDto franchiseDto);

    @Named("toMovieId")
    Set<Integer> toMovieId(Set<Movie> movies) {
        return movieConverter.toIdSet(movies);
    }

    @Named("toMovieEntity")
    Set<Movie> toMovieEntity(Set<Integer> ids) {
        return movieConverter.toEntitySet(ids);
    }
}
