package nl.mrchoi.movieapi.mappings;

import nl.mrchoi.movieapi.models.Movie;
import nl.mrchoi.movieapi.services.MovieService;
import org.springframework.stereotype.Component;

@Component
public class IdSetMovieSetConverter extends
        IdSetEntitySetConverter<Integer, Movie, MovieService> {
    public IdSetMovieSetConverter(MovieService service) {
        super(service);
    }
}
