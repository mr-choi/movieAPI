package nl.mrchoi.movieapi.mappings;

import lombok.AllArgsConstructor;
import nl.mrchoi.movieapi.models.Identifiable;
import nl.mrchoi.movieapi.services.CrudService;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * Generic converter meant to convert a set of ids into a set of database entities and vice versa.
 * @param <ID> The id data type of the entity.
 * @param <T> The type of entity.
 * @param <S> The type of service used to perform business logic with the entity
 */
@AllArgsConstructor
public abstract class IdSetEntitySetConverter<
        ID,
        T extends Identifiable<ID>,
        S extends CrudService<ID, T, ? extends JpaRepository<T,ID>>> {

    private S service;
    public Set<ID> toIdSet(Set<T> entities) {
        return (entities == null) ? null : entities.stream().map(Identifiable::getId).collect(Collectors.toSet());
    }
    public Set<T> toEntitySet(Set<ID> ids) {
        return (ids == null) ? null : ids.stream().map(this.service::findById).collect(Collectors.toSet());
    }
}
