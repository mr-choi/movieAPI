package nl.mrchoi.movieapi.mappings;

import nl.mrchoi.movieapi.models.Character;
import nl.mrchoi.movieapi.services.CharacterService;
import org.springframework.stereotype.Component;

@Component
public class IdSetCharacterSetConverter extends
        IdSetEntitySetConverter<Integer, Character, CharacterService> {
    public IdSetCharacterSetConverter(CharacterService service) {
        super(service);
    }
}
