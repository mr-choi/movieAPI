package nl.mrchoi.movieapi.mappings;

/**
 * Generic mapper that is implemented/extended by any class/interface that performs conversion between DTOs and database
 * entities.
 * @param <DTO> The type of DTO
 * @param <T> The type of Entity
 */
public interface DtoEntityMapper<DTO,T> {
    DTO toDto(T entity);
    T toEntity(DTO dto);
}
