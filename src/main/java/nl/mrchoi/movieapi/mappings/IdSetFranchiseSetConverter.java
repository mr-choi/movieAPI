package nl.mrchoi.movieapi.mappings;

import nl.mrchoi.movieapi.models.Franchise;
import nl.mrchoi.movieapi.services.FranchiseService;
import org.springframework.stereotype.Component;

@Component
public class IdSetFranchiseSetConverter extends
        IdSetEntitySetConverter<Integer, Franchise, FranchiseService> {
    public IdSetFranchiseSetConverter(FranchiseService service) {
        super(service);
    }
}
