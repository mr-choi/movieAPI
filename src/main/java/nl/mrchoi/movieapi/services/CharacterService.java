package nl.mrchoi.movieapi.services;

import nl.mrchoi.movieapi.models.Character;
import nl.mrchoi.movieapi.repositories.CharacterRepository;
import org.springframework.stereotype.Service;

@Service
public class CharacterService extends CrudService<Integer, Character, CharacterRepository> {
    public CharacterService(CharacterRepository repository) {
        super(repository);
    }
}
