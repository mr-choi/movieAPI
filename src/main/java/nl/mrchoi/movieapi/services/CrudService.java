package nl.mrchoi.movieapi.services;

import lombok.AllArgsConstructor;
import nl.mrchoi.movieapi.models.Identifiable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;

/**
 * Generic service that acts as layer between crud controller and the repository.
 * @param <ID> The data type of the id used by the involved entity.
 * @param <T> The type of entity.
 * @param <R> The type of repository used by the service.
 */
@AllArgsConstructor
public abstract class CrudService<ID, T extends Identifiable<ID>, R extends JpaRepository<T, ID>> {

    protected R repository;

    public T findById(ID id) {
        return repository.findById(id).orElseThrow(() -> {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        });
    }
    public Collection<T> findAll() {
        return repository.findAll();
    }
    public T add(T entity) {
        if (entity.getId() != null) throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        return repository.save(entity);
    }
    public T update(T entity) {
        if (!repository.existsById(entity.getId()))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return repository.save(entity);
    }
    public void deleteById(ID id) {
        repository.deleteById(id);
    }
}
