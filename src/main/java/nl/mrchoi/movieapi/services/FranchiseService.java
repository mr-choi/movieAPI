package nl.mrchoi.movieapi.services;

import nl.mrchoi.movieapi.models.Character;
import nl.mrchoi.movieapi.models.Franchise;
import nl.mrchoi.movieapi.models.Movie;
import nl.mrchoi.movieapi.repositories.FranchiseRepository;
import nl.mrchoi.movieapi.repositories.MovieRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashSet;
import java.util.Set;

@Service
public class FranchiseService extends CrudService<Integer, Franchise, FranchiseRepository> {

    private final MovieRepository movieRepository;

    public FranchiseService(FranchiseRepository repository, MovieRepository movieRepository) {
        super(repository);
        this.movieRepository = movieRepository;
    }

    @Transactional
    public Set<Movie> getMoviesFromFranchiseId(Integer id) {
        return repository.findById(id).orElseThrow(() -> {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }).getMovies();
    }

    @Transactional
    public Set<Character> getCharactersFromFranchiseId(Integer id) {
        Set<Character> characters = new HashSet<>();
        repository.findById(id).orElseThrow(() -> {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }).getMovies().forEach(m -> {
            characters.addAll( m.getCharacters());
        });
        return characters;
    }

    @Transactional
    public Franchise setMoviesForFranchiseId(Integer franchiseId, Set<Integer> movieIds) {
        Franchise franchise = repository.findById(franchiseId).orElseThrow(() -> {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        });
        franchise.getMovies().forEach(movie -> movie.setFranchise(null));
        franchise.getMovies().clear();
        movieIds.forEach(m -> movieRepository.findById(m).ifPresent(movie -> {
            movie.setFranchise(franchise);
            franchise.getMovies().add(movie);
        }));
        return franchise;
    }
}
