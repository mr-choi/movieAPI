package nl.mrchoi.movieapi.services;

import nl.mrchoi.movieapi.models.Character;
import nl.mrchoi.movieapi.models.Movie;
import nl.mrchoi.movieapi.repositories.CharacterRepository;
import nl.mrchoi.movieapi.repositories.MovieRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashSet;
import java.util.Set;

@Service
public class MovieService extends CrudService<Integer, Movie, MovieRepository> {

    private final CharacterRepository characterRepository;
    public MovieService(MovieRepository repository, CharacterRepository characterRepository) {
        super(repository);
        this.characterRepository = characterRepository;
    }

    @Transactional
    public Set<Character> getCharactersFromMovieId(Integer id) {
        return repository.findById(id).orElseThrow(() -> {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }).getCharacters();
    }
    @Transactional
    public Movie setCharactersForMovieId(Integer movieId, Set<Integer> characterIds) {
        Movie movie = repository.findById(movieId).orElseThrow(() -> {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        });
        movie.getCharacters().clear();
        characterIds.forEach(c -> {
            characterRepository.findById(c).ifPresent(character -> movie.getCharacters().add(character));
        });
        return movie;
    }
}
